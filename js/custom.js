$(document).ready(function(){
  var curentCardValue = 0, prevCardValue = 0;

  var cards = $('.cards span');

//  randomly generate pair of numbers and insert in html
  var uniqueRandoms = [];
  var numRandoms = 5;
  function makeUniqueRandom() {
    // refill the array if needed
    if (!uniqueRandoms.length) {
      for (var i = 0; i < numRandoms; i++) {
        uniqueRandoms.push(i);
      }
    }
    var index = Math.floor(Math.random() * uniqueRandoms.length);
    var val = uniqueRandoms[index];

    // now remove that value from the array
    uniqueRandoms.splice(index, 1);

    return val;

  }

  for (i = 0; i < cards.length; i++ ){
    $(cards[i]).text(makeUniqueRandom());
  }

  var cardsComparison = function(curentCardValue, prevCardValue){
    if (curentCardValue === prevCardValue) {
      return true;
    } else {
      return false;
    }
  };

  var openCardsFields = function(){
    $('.cards-field .active').addClass('opend');
  };

  var closeCardsFields = function(){
    setTimeout(function(){
    $('.cards-field .active').removeClass('active')}, 1000);
  };

//  get card value on click for curent and prev cards
  $('.cards').click(function(){
    if(curentCardValue === 0) {
      curentCardValue = $(this).text();
      $(this).addClass('active');
    } else {
      prevCardValue = curentCardValue;
      curentCardValue = $(this).text();
      $(this).addClass('active');
//    call function cardsComparison
      console.log(cardsComparison(curentCardValue, prevCardValue));
      if (cardsComparison(curentCardValue, prevCardValue)) {
//      display two clicked cards and set to zero curentCardValue prevCardValue values
        openCardsFields();
        $('.cards-field .active').removeClass('active');
        curentCardValue = 0;
        prevCardValue = 0;
      } else {
//      hide two clicked cards and set to zero curentCardValue prevCardValue values
        closeCardsFields();
        curentCardValue = 0;
        prevCardValue = 0;
      }
    }
    console.log('curentCardValue:' + curentCardValue + '\n' + 'prevCardValue:' + prevCardValue);
  });

});